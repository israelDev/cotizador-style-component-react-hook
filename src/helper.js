export function diferenciaYear (year){
    return new Date().getFullYear() - year;
}

export function calcularMarca (marca){
    let incremento = 0
    switch(marca){
        case('americano'):
            incremento = 1.15
            break;
        case('europeo'):
            incremento = 1.3
            break;
        case('asiatico'):
            incremento = 1.05
            break;
        default:
            break;
    }

    return incremento
}

export function calculoPlan (plan){
    return (plan === 'basico'?  1.25 :  1.5);
}

export function primeraMayuscula (texto){
    return texto.charAt(0).toUpperCase() + texto.slice(1);
}
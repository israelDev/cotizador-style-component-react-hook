import React, { useState } from 'react';
import { diferenciaYear, calcularMarca, calculoPlan } from '../helper'
import PropTypes from 'prop-types'
import styled from '@emotion/styled';

const Campo = styled.div`
    display: flex;
    margin-bottom : 1rem;
    align-items : center;
`

const Label = styled.label`
    flex: 0 0 100px;

`

const Select = styled.select`
    display : block;
    width : 100%;
    padding : 1rem;
    border: 1px solid #e1e1e1;
    -webkit-appearance : nonen;
`

const InputRadio = styled.input`
    margin: 0 1rem;

`

const Boton = styled.button`
    background-color: #00838f;
    font-size : 16px;
    width : 100%;
    padding: 1rem;
    color : #fff;
    text-transform : uppercase;
    font-weight : bold;
    border: none;
    transition: background-color .3s ease;
    margin-top: 2rem; 

    &:hover{
        background-color: #26c6da;
        cursor: pointer;

    }

`

const Error = styled.div`
    background-color: red;
    color : white;
    padding : 1rem;
    margin-bottom :1rem;
    
`
const Formulario = ({ guardarResumen, guardarCargando }) => {

    const [datos, guardarDatos] = useState({
        marca: '',
        year: '',
        plan: ''
    }
    );

    const [error, guardarError] = useState(false)
    const { marca, year, plan } = datos;

    const obtenerDatos = e => {
        guardarDatos({
            ...datos,
            [e.target.name]: e.target.value
        })
    }


    const cotizarSeguro = e => {
        let resultado = 2000

        e.preventDefault();

        if (marca.trim() === '' || year.trim() === '' || plan.trim() === '') {
            guardarError(true)
            return
        }

        // calculando los años 

        const diferencia = diferenciaYear(year)
        resultado -= ((diferencia * 3) * resultado) / 100

        //calculando las marcas
        // Americano 15
        // Asiatico 5
        // Europeo 30

        resultado = calcularMarca(marca) * resultado

        // calculo del plan 
        resultado = parseFloat(calculoPlan(plan) * resultado).toFixed(2)

        guardarCargando(true)


        setTimeout(()=>{

            guardarCargando(false)
            guardarResumen({
                cotizacion: Number(resultado),
                datos
            })
            guardarError(false)

        },3000)




        
    }

    return (
        <form
            onSubmit={cotizarSeguro}
        >
            {error ? <Error>Todos los campos son obligatorios</Error> : null};
            <Campo>
                <Label> Marca </Label>
                <Select
                    name='marca'
                    value={marca}
                    onChange={obtenerDatos}
                >
                    <option value=""> --- Seleccione ---</option>
                    <option value="americano"> --- Americano ---</option>
                    <option value="europeo"> --- Europeo ---</option>
                    <option value="asiatico"> --- Asiatico ---</option>
                </Select>
            </Campo>

            <Campo>
                <Label> Año </Label>
                <Select
                    name='year'
                    value={year}
                    onChange={obtenerDatos}

                >
                    <option value="">-- Seleccione --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </Select>
            </Campo>

            <Campo>
                <Label> Plan </Label>
                <InputRadio
                    type="radio"
                    name="plan"
                    value="basico"
                    checked={plan === "basico"}
                    onChange={obtenerDatos}
                /> Basico
                <InputRadio
                    type="radio"
                    name="plan"
                    value="completo"
                    checked={plan === "completo"}
                    onChange={obtenerDatos}
                /> Completo
            </Campo>
            <Boton type="submit">Cotizar</Boton>

        </form>

    );
}

Formulario.prototype = {
    guardarResumen : PropTypes.func.isRequired,
    guardarCargando : PropTypes.func.isRequired,
}


export default Formulario;
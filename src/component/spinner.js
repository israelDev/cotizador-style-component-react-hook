import React from 'react';
import './Spinner.css'

import styled from '@emotion/styled';

const Centrar = styled.div`
    padding-left: 43%;
`

const Spinner = () => {
    return (
        <Centrar className="row">
        <div className="sk-chase">
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
            <div className="sk-chase-dot"></div>
        </div>
        </Centrar>


    );
}

export default Spinner;
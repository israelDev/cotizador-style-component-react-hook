import React from 'react';
import styled from '@emotion/styled';
import {primeraMayuscula} from '../helper'
import PropTypes from 'prop-types'



const ContendorResumen = styled.div`
    padding : 1rem;
    text-align : center;
    background-color: #00838F;
    color: #FFF;
    margin-top : 1rem; 
`;

const Resumen = ({ datos }) => {

    const { marca, plan, year } = datos
    if(marca ===''|| plan ===''|| year ==='') return null
    return (

        <ContendorResumen>
            <h2>resumen de cotizacion</h2>
            <ul>
                <li> marca : {primeraMayuscula(marca)}</li>
                <li> año :{year}</li>
                <li> plan : {primeraMayuscula(plan)}</li>
            </ul>
        </ContendorResumen>
    );
}

Resumen.prototype = {
    cotizacion : PropTypes.object.isRequired
}
export default Resumen;
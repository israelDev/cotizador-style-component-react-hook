import React, { Fragment, useState } from 'react';
import Header from './component/Header'
import Formulario from './component/Formulario'
import Resumen from './component/Resumen'
import Resultado from './component/Resultado'
import Spinner from './component/spinner'
import PropTypes from 'prop-types'



import styled from '@emotion/styled';

const Contenedor = styled.div`
    max-width : 600px;
    margin : 0 auto;
`;

const ContenedorFormulario = styled.div`
    background-color : #ffffff;
    padding : 3rem;
`;


function App() {

  const [resumen, guardarResumen] = useState({
    cotizacion: 0,
    datos: {
      marca: '',
      year: '',
      plan: ''
    }
  })

  const [cargando, guardarCargando] = useState(false)
  const { datos, cotizacion } = resumen
  return (
    <Fragment>
      <Contenedor>
        <Header
          titulo={'cotizador de autos'}
        />
        <ContenedorFormulario>
          <Formulario
            guardarResumen={guardarResumen}
            guardarCargando={guardarCargando}
          />
          {cargando ? <Spinner /> :
            <Fragment>
              <Resumen datos={datos} />
              <Resultado cotizacion={cotizacion} />
            </Fragment>

          }


        </ContenedorFormulario>
      </Contenedor>

    </Fragment>
  );
}

Formulario.prototype = {
  guardarResumen : PropTypes.func.isRequired,
  guardarCargando : PropTypes.func.isRequired,
}



export default App;
